public interface SymmetricThreeDShape extends Positionable, ThreeDShape {
    Point center();
    double volume();
}
