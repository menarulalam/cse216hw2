import java.util.List;

public class Sphere implements Positionable, SymmetricThreeDShape {
    private double radius;
    private ThreeDPoint center;

    public double getRadius() {
        return radius;
    }

    public ThreeDPoint getCenter() {
        return center;
    }

    @Override
    public Point center() {
        return center;
    }

    @Override
    public double volume() {
        return DoubleCompare.round(4*Math.PI*Math.pow(radius, 3)/3);
    }

    public double surfaceArea() {
        return DoubleCompare.round(4*Math.PI*Math.pow(radius, 2));
    }

    @Override
    public void setPosition(List<? extends Point> points) throws IllegalArgumentException {
        if(points.size()==0||!(points.get(0) instanceof ThreeDPoint))
            throw new IllegalArgumentException();
        center = (ThreeDPoint)points.get(0);
    }

    @Override
    public List<? extends Point> getPosition() {
        return null;
    }
    public Sphere(double centerx, double centery, double centerz, double radius) {
        this.center = new ThreeDPoint(centerx, centery, centerz);
        this.radius = radius;
    }

    @Override
    public int compareTo(ThreeDShape o) {
        return DoubleCompare.compare(this.volume(), o.volume());
    }
    public static Sphere random(){
        return new Sphere(ThreeDPoint.randomDouble(), ThreeDPoint.randomDouble(), ThreeDPoint.randomDouble(), ThreeDPoint.randomDouble());
    }
}
