/**
 * An unmodifiable point in the three-dimensional space. The coordinates are specified by exactly three doubles (its
 * <code>x</code>, <code>y</code>, and <code>z</code> values).
 */
public class ThreeDPoint implements Point {
    private static final double MIN_RANDOM_DOUBLE = -100.0;
    private static final double MAX_RANDOM_DOUBLE = 100.0;
    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public double getZ() {
        return z;
    }

    private double x;
    private double y;
    private double z;
    public ThreeDPoint(double x, double y, double z) {
        this.x=x;
        this.z=z;
        this.y=y;
    }

    /**
     * @return the (x,y,z) coordinates of this point as a <code>double[]</code>.
     */
    @Override
    public double[] coordinates() {
        return new double[]{x, y, z};
    }

    public double distanceTo(Point a) throws IllegalArgumentException {
        if(!(a instanceof ThreeDPoint))
            throw new IllegalArgumentException();
        ThreeDPoint a1=(ThreeDPoint)a;
        return Math.sqrt((a1.x-this.x)*(a1.x-this.x)+(a1.y-this.y)*(a1.y-this.y)+(a1.z-this.z)*(a1.z-this.z));
    }

    public Point average(Point a) {
        ThreeDPoint a1 = (ThreeDPoint)a;
        return new ThreeDPoint(DoubleCompare.round((this.x+a1.x)/2), DoubleCompare.round((this.y+a1.y)/2), DoubleCompare.round((this.z+a1.z)/2));
    }

    public Point snap() {
        return new ThreeDPoint(Math.round(x), Math.round(y), Math.round(z));
    }
    public ThreeDPoint add(ThreeDPoint p){
        return new ThreeDPoint(p.x+this.x, p.y+this.y, p.z+this.z);
    }
    public ThreeDPoint add(double d1, double d2, double d3){
        return new ThreeDPoint(this.x+d1, this.y+d2, this.z+d3);
    }
    public static double randomDouble(){
        return (MAX_RANDOM_DOUBLE-MIN_RANDOM_DOUBLE)*Math.random()+MIN_RANDOM_DOUBLE;
    }
    public static ThreeDPoint random(){
        return new ThreeDPoint(randomDouble(), randomDouble(), randomDouble());
    }
    public String toString(){
        return "("+ DoubleCompare.round(this.x)+", "+DoubleCompare.round(this.y)+", "+DoubleCompare.round(this.z)+")";
    }
}
