import java.util.*;

public class Ordering {
    static class XLocationComparator implements Comparator<TwoDShape> {
        @Override public int compare(TwoDShape o1, TwoDShape o2) {
            double s1; double s2;
            if(o1 instanceof Quadrilateral)
                s1=((Quadrilateral)o1).getX();
            else
                s1=((Circle)o1).getX();
            if(o2 instanceof Quadrilateral)
                s2=((Quadrilateral)o2).getX();
            else
                s2=((Circle)o2).getX();
            return DoubleCompare.compare(s1, s2);
        }
    }

    static class AreaComparator implements Comparator<SymmetricTwoDShape> {
        @Override public int compare(SymmetricTwoDShape o1, SymmetricTwoDShape o2) {
            return DoubleCompare.compare(o1.area(), o2.area());
        }
        }


    static class SurfaceAreaComparator implements Comparator<ThreeDShape> {
        @Override public int compare(ThreeDShape o1, ThreeDShape o2) {
            double s1; double s2;
            if(o1 instanceof Sphere)
                s1 = ((Sphere) o1).surfaceArea();
            else
                s1 = ((Cuboid) o1).surfaceArea();
            if(o2 instanceof Sphere)
                s2 = ((Sphere) o2).surfaceArea();
            else
                s2 = ((Cuboid) o2).surfaceArea();
            return DoubleCompare.compare(s1, s2);
        }
    }

    // TODO: there's a lot wrong with this method. correct it so that it can work properly with generics.
     static  <T> void copy(Collection<T> source, Collection<? super T> destination) {
        destination.addAll(source);
    }

    public static void main(String[] args) {
        List<TwoDShape>          shapes          = new ArrayList<>();
        List<SymmetricTwoDShape> symmetricshapes = new ArrayList<>();
        List<ThreeDShape>        threedshapes    = new ArrayList<>();

        /*
         * uncomment the following block and fill in the "..." constructors to create actual instances. If your
         * implementations are correct, then the code should compile and yield the expected results of the various
         * shapes being ordered by their smallest x-coordinate, area, volume, surface area, etc. */


        symmetricshapes.add(new Rectangle( 0, 0, 0, 4, 4, 4, 4, 0));
        symmetricshapes.add(new Square((List<TwoDPoint>) symmetricshapes.get(0).getPosition()));
        symmetricshapes.add(new Circle(0, 0, 4));

        copy(symmetricshapes, shapes); // note-1 //
        try {
            shapes.add(new Quadrilateral(new ArrayList<>()));
        }
        catch(Exception ignored){};

        // sorting 2d shapes according to various criteria
        shapes.sort(new XLocationComparator());
        symmetricshapes.sort(new XLocationComparator());
        symmetricshapes.sort(new AreaComparator());

        // sorting 3d shapes according to various criteria
        Collections.sort(threedshapes);
        threedshapes.sort(new SurfaceAreaComparator());

        /*
         * if your changes to copy() are correct, uncommenting the following block will also work as expected note that
         * copy() should work for the line commented with 'note-1' while at the same time also working with the lines
         * commented with 'note-2' and 'note-3'. */


        List<Number> numbers = new ArrayList<>();
        List<Double> doubles = new ArrayList<>();
        Set<Square>        squares = new HashSet<>();
        Set<Quadrilateral> quads   = new LinkedHashSet<>();

        copy(doubles, numbers); // note-2 //
        copy(squares, quads);   // note-3 //

    }
}


