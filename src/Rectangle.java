import java.util.Arrays;
import java.util.List;

public class Rectangle extends Quadrilateral implements SymmetricTwoDShape {
    public Rectangle(double... vertices) throws IllegalArgumentException{
        super(vertices);
    }
    public Rectangle(List<TwoDPoint> vertices) throws IllegalArgumentException {
        super(vertices);
    }

    /**
     * The center of a rectangle is calculated to be the point of intersection of its diagonals.
     *
     * @return the center of this rectangle.
     */
    @Override
    public Point center() { List<TwoDPoint> points = this.getPosition(); return points.get(0).average(points.get(2));
    }

    @Override
    public boolean isMember(List<? extends Point> vertices) {
        if(!super.isMember(vertices))
            return false;
        for(Point p:vertices)
            if(!(p instanceof TwoDPoint))
                return false;
        double[] sideLengths = new double[]{((TwoDPoint)vertices.get(0)).distanceTo(vertices.get(1)),((TwoDPoint)vertices.get(1)).distanceTo(vertices.get(2)), ((TwoDPoint)vertices.get(2)).distanceTo(vertices.get(3)), ((TwoDPoint)vertices.get(3)).distanceTo(vertices.get(0)) };
        double[] diagonalLengths = new double[]{((TwoDPoint)vertices.get(0)).distanceTo(vertices.get(2)), ((TwoDPoint)vertices.get(1)).distanceTo(vertices.get(3))};
        return (DoubleCompare.compare(sideLengths[0], sideLengths[2])==0&&DoubleCompare.compare(sideLengths[1], sideLengths[3])==0&&DoubleCompare.compare(diagonalLengths[0], diagonalLengths[1])==0);
    }

    @Override
    public double getX() {
        return super.getX();
    }

    @Override
    public double area() {
        double[] arr = this.getSideLengths();
        return  DoubleCompare.round(arr[0]*arr[1]);
    }
    public String toString(){
        return super.toString()+" Area: "+area();
    }
}
