import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * An unmodifiable point in the standard two-dimensional Euclidean space. The coordinates of such a point is given by
 * exactly two doubles specifying its <code>x</code> and <code>y</code> values.
 */
public class TwoDPoint implements Point {
    private double x;
    private double y;

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public TwoDPoint(double x, double y) {
        this.x=x; this.y=y;
    }

    /**
     * @return the coordinates of this point as a <code>double[]</code>.
     */
    @Override
    public double[] coordinates() {
        return new double[]{x, y};
    }

    public double distanceTo(Point a) throws IllegalArgumentException {
        if(!(a instanceof  TwoDPoint))
            throw new IllegalArgumentException();
        TwoDPoint a1 = (TwoDPoint)a;
        return Math.sqrt((this.x-a1.x)*(this.x-a1.x)+(this.y-a1.y)*(this.y-a1.y));
    }

    public Point average(Point a) throws IllegalArgumentException {
        if(!(a instanceof  TwoDPoint))
            throw new IllegalArgumentException();
        TwoDPoint a1 = (TwoDPoint)a;
        return new TwoDPoint(DoubleCompare.round(this.x+a1.x)/2 , DoubleCompare.round(this.y+a1.y)/2);
    }

    public Point snap() {
        return new TwoDPoint(Math.round(x), Math.round(y));
    }

    /**
     * Returns a list of <code>TwoDPoint</code>s based on the specified array of doubles. A valid argument must always
     * be an even number of doubles so that every pair can be used to form a single <code>TwoDPoint</code> to be added
     * to the returned list of points.
     *
     * @param coordinates the specified array of doubles.
     * @return a list of two-dimensional point objects.
     * @throws IllegalArgumentException if the input array has an odd number of doubles.
     */
    public static List<TwoDPoint> ofDoubles(double... coordinates) throws IllegalArgumentException {
        if(coordinates.length%2==1||coordinates.length==0)
            throw new IllegalArgumentException();
        List<TwoDPoint> points = new ArrayList<>();
        for(int i=0; i<coordinates.length; i+=2){
            points.add(new TwoDPoint(coordinates[i], coordinates[i+1]));
        }
        return points;
    }

    @Override
    public boolean equals(Object obj) {
        if(!(obj instanceof TwoDPoint))
            return false;
        TwoDPoint p = (TwoDPoint)obj;
        return DoubleCompare.compare(p.getX(), this.getX())==0&&DoubleCompare.compare(p.getY(), this.getY())==0;
    }
    public String toString(){
        return "("+ DoubleCompare.round(this.x)+", "+DoubleCompare.round(this.y)+")";
    }
}
