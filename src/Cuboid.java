import java.util.ArrayList;
import java.util.List;

// TODO : a missing interface method must be implemented in this class to make it compile. This must be in terms of volume().
public class Cuboid implements ThreeDShape {

    public ThreeDPoint[] getVertices() {
        return vertices;
    }

    private final ThreeDPoint[] vertices = new ThreeDPoint[8];

    /**
     * Creates a cuboid out of the list of vertices. It is expected that the vertices are provided in
     * the order as shown in the figure given in the homework document (from v0 to v7).
     * 
     * @param vertices the specified list of vertices in three-dimensional space.
     */
    public Cuboid(List<ThreeDPoint> vertices) {
        if (vertices.size() != 8)
            throw new IllegalArgumentException(String.format("Invalid set of vertices specified for %s",
                                                             this.getClass().getName()));
        int n = 0;
        for (ThreeDPoint p : vertices) this.vertices[n++] = p;
    }

    @Override
    public double volume() { return DoubleCompare.round((vertices[0].distanceTo(vertices[1])*vertices[0].distanceTo(vertices[3])
            *vertices[0].distanceTo(vertices[5])));
    }

    public double surfaceArea() {
        double s1 = vertices[0].distanceTo(vertices[1]);
        double s2 = vertices[0].distanceTo(vertices[3]);
        double s3 = vertices[0].distanceTo(vertices[5]);
        return DoubleCompare.round(2*(s1*s2+s2*s3+s3*s1));
    }

    @Override
    public ThreeDPoint center() {
        return (ThreeDPoint) (vertices[0].average(vertices[7]));
    }

    @Override
    public int compareTo(ThreeDShape o) {
        return DoubleCompare.compare(this.volume(), o.volume());
    }
    public static Cuboid random(){
        List<ThreeDPoint> v = new ArrayList<>();
        ThreeDPoint v2 = ThreeDPoint.random();
        double x = Math.abs(ThreeDPoint.randomDouble());
        double y = Math.abs(ThreeDPoint.randomDouble());
        double z = Math.abs(ThreeDPoint.randomDouble());
        ThreeDPoint v1 =v2.add(0, 0, z);
        ThreeDPoint v7 = v2.add(0, y, 0);
        ThreeDPoint v3 = v2.add(x, 0, 0);
        ThreeDPoint v6 = v7.add(0, 0, z);
        ThreeDPoint v0 = v1.add(x, 0, 0);
        ThreeDPoint v5 = v0.add(0, y, 0);
        ThreeDPoint v4 = v7.add(x, 0, 0);
        v.add(v0); v.add(v1); v.add(v2); v.add(v3); v.add(v4); v.add(v5); v.add(v6); v.add(v7);
        return new Cuboid(v);
    }
}
