public abstract class DoubleCompare {
    public static final double epsilon = 0.00001d;
    public static final double round =10000.0;
    public static int compare(Double a1, Double a2){
        if(Math.abs(a1-a2)<=epsilon)
            return 0;
        if(a1>a2)
            return 1;
        return -1;
    }
    public  static double round(double d){
        return Math.round(round*d)/round;
    }
    public static double[] round(double... arr){
        double[] arr1 = new double[arr.length];
        int n = 0;
        for(double d:arr){
            arr1[n++]=round(d);
        }
        return arr1;
    }

}
